package com.wyn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Dictionary;
import java.rmi.ServerException;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
// import org.apache.sling.commons.osgi.OsgiUtil;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.felix.scr.annotations.Reference;
import org.osgi.service.component.ComponentContext;
import javax.jcr.Session;
import javax.jcr.Node; 
// import org.json.*;
import java.util.UUID;
 
@SlingServlet(paths="/bin/testServlet", methods = "POST, GET", metatype=true)
public class TestServlet extends org.apache.sling.api.servlets.SlingAllMethodsServlet {

    private static final long serialVersionUID = 2598426539166789515L;
    
    private final String APP_NAME = "Hello World.v3";
      
    @Reference
    private SlingRepository repository;

    public void bindRepository(SlingRepository repository) {
        this.repository = repository; 
    }

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
        try  {
            String var1 = request.getParameter("var1");
            response.getWriter().write(APP_NAME + "(POST): var1=" + var1);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
        try  {
            //Get the submitted form data that is sent from the
            //CQ web page  
            //String firstName = request.getParameter("firstName");
            //Encode the submitted form data to JSON
            // JSONObject obj=new JSONObject();
            // obj.put("id",id);
            // obj.put("firstname",firstName);
            // obj.put("lastname",lastName);
            // obj.put("address",address);
            // obj.put("cat",cat);
            // obj.put("state",state);
            // obj.put("details",details);
            // obj.put("date",date);
            // obj.put("city",city);
            //Get the JSON formatted data    
            // String jsonData = obj.toJSONString();
            //Return the JSON formatted data

            String var1 = request.getParameter("var1");
            response.getWriter().write(APP_NAME + ": var1=" + var1);
      }
      catch(Exception e) {
          e.printStackTrace();
      }
    }
}
