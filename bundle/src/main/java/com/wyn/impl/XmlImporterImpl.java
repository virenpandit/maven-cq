package com.wyn.impl;

import javax.jcr.Repository;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.Resource;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.Page;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.apache.sling.jcr.resource.JcrResourceUtil;
import javax.jcr.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.Session;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import java.io.File;

import com.wyn.XmlImporter;

/**
 * One implementation of the {@link XmlImporter}. Note that
 * the repository is injected, not retrieved.
 *      http://localhost:4502/bin/xmlimporter
 */
@SlingServlet(
    paths = "/bin/xmlimporter",
    methods = "GET",
    metatype = true,
    label = "WHG XmlImporter Servlet")
public class XmlImporterImpl extends SlingAllMethodsServlet {

    /* CHANGE ME!!!!! */

    private final static String JCR_CONTENT_LOCATION = "/content/geometrixx";
    private final static String PAGE_TEMPLATE_NAME = "/apps/geometrixx/templates/homepage";
    private final static String JCR_XML_DATA_LOCATION = "/var/brandstandards/data/brandstandards.xml/jcr:content";

    /* End: CHANGE ME */


    private final static String VERSION = "v3.4";
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference
    private SlingRepository repository;

    public String getRepositoryName() {
        return repository.getDescriptor(Repository.REP_NAME_DESC);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        try {
            log.info("Inside doGet()");
            PrintWriter writer = response.getWriter();
            ResourceResolver resolver = request.getResourceResolver();
            Resource res = resolver.getResource(JCR_XML_DATA_LOCATION);
            javax.jcr.Node node = res.adaptTo(Node.class);
            String data = node.getProperty("jcr:data").getValue().getString();
            InputStream is = node.getProperty("jcr:data").getStream();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);
            log.info("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("item");
            PageManager pageManager =  resolver.adaptTo(PageManager.class);
            int pageid=1;
            writer.write("<p><h1>Importing Xml data into pages</h1>");
            for (int ctr = 0; ctr < nList.getLength(); ctr++) {
                org.w3c.dom.Node nNode = (org.w3c.dom.Node)nList.item(ctr);
                if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String title = eElement.getElementsByTagName("brandstandardstitle").item(0).getTextContent();
                    String manualnumber = eElement.getElementsByTagName("manualnumber").item(0).getTextContent();
                    String category = eElement.getElementsByTagName("category").item(0).getTextContent();
                    String section = eElement.getElementsByTagName("section").item(0).getTextContent();
                    String standardbody = eElement.getElementsByTagName("standardbody").item(0).getTextContent();

                    log.info("title: " + title);
                    writer.write("<p>Creating page: " + title);
                    response.flushBuffer();

                    Page page = pageManager.create(JCR_CONTENT_LOCATION, "page"+pageid, PAGE_TEMPLATE_NAME, title);
                    pageid++;
                    javax.jcr.Node pageNode = page.adaptTo(javax.jcr.Node.class);
                    javax.jcr.Node pageNodeData = pageNode.getNode("jcr:content");
                    pageNodeData.setProperty("manualnumber", manualnumber);
                    pageNodeData.setProperty("category", category);
                    pageNodeData.setProperty("section", section);
                    pageNodeData.setProperty("standardbody", standardbody);
                    pageNodeData.save();
                }
            }
            writer.write("<p><b>Complete! Total pages created: [" + pageid + "]</b>");
            response.flushBuffer();
        } catch(Exception ex) {
            log.error(ex.toString());
        }

        log.info("Exiting doGet()");
    }
}
