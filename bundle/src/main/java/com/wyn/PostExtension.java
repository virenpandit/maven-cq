package com.wyn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.RepositoryException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.servlets.post.PostOperation;
import org.apache.sling.servlets.post.AbstractPostOperation;
import org.apache.sling.servlets.post.Modification;
import org.apache.sling.servlets.post.PostResponse;

@Component(immediate = true)
@Service
@Property(name = PostOperation.PROP_OPERATION_NAME, value = "wyn_post")
public class PostExtension extends AbstractPostOperation{

    @Override
    protected void doRun(SlingHttpServletRequest request, PostResponse response, List<Modification> changes) throws RepositoryException {
        
        log("Inside doRun().v3.4");
        
        String var1 = request.getParameter("var1");
        log("var1=" + var1);

        request.setAttribute("var1", "PREFIX");
        
        log("ItemPath=" + getItemPath(request));
        
        Resource res = request.getResource();
        log("Resource=" + res);
        
        // run(request, response, null);
        
        final Resource r = request.getResource();
        final Node n = r.adaptTo(Node.class);
        try {
            response.setPath(r.getPath());
            response.setTitle("Content modified by " + getClass().getSimpleName());
            n.setProperty(getClass().getName(), "Operation was applied to " + n.getPath());
            n.getSession().save();
        } catch(RepositoryException re) {
            log(re);
            re.printStackTrace();
        }
        
        // try {
            // RequestDispatcher dispatcher = request.getRequestDispatcher(res);
            // dispatcher.forward((HttpServletRequest)request, (HttpServletResponse)response);  
        // } catch(Exception ex) {
            // log(ex);
            // ex.printStackTrace();
        // }        
    }

    
    private void log(Object msg) {
        System.out.println("Servlet-PostExtension: " + msg);
    }
    
    // protected SocialOperationResult performOperation(SlingHttpServletRequest req) throws OperationException {
        // final String statusToSet = req.getParameter("status");
        // final Resource idea = req.getResource();
        // final ValueMap props = idea.adaptTo(ValueMap.class);
        // final String[] tags = props.get("cq:tags", new String[]{});
        // final List<String> tagList = new ArrayList<String>();
        // for(String tag: tags) {
            // if(!tag.startsWith("acmeideas:")) {
                // tagList.add(tag);
            // }
        // }
        // tagList.add(statusToSet);
        // Map<String,Object> updates = new HashMap<String,Object>();
        // updates.put("cq:tags", tagList.toArray(new String[]{}));
        // Resource updatedResource = forumService.update(req.getResource(), updates, null, req.getResourceResolver().adaptTo(Session.class));
        // return new SocialOperationResult(this.getSocialComponentForResource(updatedResource, req), 200, updatedResource.getPath());
    // }

    // private SocialComponent getSocialComponentForResource(Resource newProject, SlingHttpServletRequest request) {
        // if (newProject == null) {
            // return null;
        // }
        // final SocialComponentFactory factory = this.srf.getSocialComponentFactory(newProject);
        // return factory.getSocialComponent(newProject, request);
    // }

}