package com.wyn;

/**
 * A simple service interface
 */
public interface XmlImporter {

    /**
     * @return the name of the underlying JCR repository implementation
     */
    public String getRepositoryName();

}